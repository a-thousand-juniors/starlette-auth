class StarlauthException(Exception):
    pass


class InvalidToken(StarlauthException):
    pass


class ExpiredToken(StarlauthException):
    pass
