import databases
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse

from .settings import DATABASE_URL, TESTING

# -----------------
# database
# -----------------
if TESTING:
    db = databases.Database(DATABASE_URL, force_rollback=True)
else:
    db = databases.Database(DATABASE_URL)


# -----------------
# exception handlers
# -----------------
async def http_exception_handler(_request, exc):
    return JSONResponse(status_code=exc.status_code, content=exc.detail)


exception_handlers = {HTTPException: http_exception_handler}
