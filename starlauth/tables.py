from sqlalchemy import Column, DateTime, MetaData, String, Table

from . import utils

metadata = MetaData()

users = Table(
    "users",
    metadata,
    Column("email", String(255), primary_key=True),
    Column("username", String(16), nullable=False, unique=True, index=True),
    Column("casefolded_username", String(16), nullable=False, unique=True, index=True),
    Column("passphrase", String(64), nullable=False),
    Column("display_name", String(48), nullable=False),
    Column(
        "created_at", DateTime(timezone=True), nullable=False, default=utils.get_utcnow
    ),
)
