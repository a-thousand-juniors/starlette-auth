import time

import itsdangerous

from . import settings


def get_timestamp() -> int:
    return int(time.time())


class TimestampSigner(itsdangerous.TimestampSigner):
    """
    Mock-friendly subclass of `itsdangerous.TimestampSigner`.
    """

    def get_timestamp(self) -> int:
        """Returns the current timestamp. The function must return an integer."""
        return get_timestamp()


class URLSafeTimedSerializer(itsdangerous.URLSafeTimedSerializer):
    default_signer = TimestampSigner


EMAIL_CONFIRMATION_SALT = "email confirmation salt"
email_confirmation_token_generator = URLSafeTimedSerializer(
    str(settings.SECRET_KEY), salt=EMAIL_CONFIRMATION_SALT
)
