from starlette.applications import Starlette

from . import settings
from .resources import db, exception_handlers
from .routes import routes

app = Starlette(
    debug=settings.DEBUG,
    routes=routes,
    on_startup=[db.connect],
    on_shutdown=[db.disconnect],
    exception_handlers=exception_handlers,
)
