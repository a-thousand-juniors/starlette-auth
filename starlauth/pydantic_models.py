import re
import unicodedata

from confusable_homoglyphs import confusables
from email_validator import EmailNotValidError, validate_email
from pydantic import BaseModel, validator  # noqa: E0611
from zxcvbn import zxcvbn

from .constants.validation import RESERVED_NAMES


class UserModel(BaseModel):
    display_name: str
    email: str
    username: str
    passphrase: str

    @validator("email")
    def validate_email(cls, email):  # noqa: E0213
        try:
            normalized_email = validate_email(email, check_deliverability=False).email
        except EmailNotValidError as exc:
            raise ValueError(str(exc)) from exc

        if any(confusables.is_dangerous(part) for part in normalized_email.split("@")):
            raise ValueError("This email cannot be registered.")

        return normalized_email

    @validator("username")
    def validate_username(cls, username):  # noqa: E0213
        if not re.compile(r"\A[\w]+\Z", flags=re.ASCII).search(username):
            raise ValueError(
                "username may contain ONLY alphanumeric characters "
                "(a to z, A to Z, 0 to 9) and the hyphen (-)."
            )

        if len(username) < 4:
            raise ValueError("username may NOT be shorter than 4 characters.")

        if len(username) > 16:
            raise ValueError("username may NOT be longer than 16 characters.")

        normalized_username = unicodedata.normalize("NFKC", username)

        if normalized_username in RESERVED_NAMES:
            raise ValueError("This username cannot be registered.")

        return normalized_username

    @validator("display_name")
    def validate_display_name(cls, display_name):  # noqa: E0213
        stripped = display_name.strip()

        if len(stripped) < 2:
            raise ValueError(
                "display name may NOT be shorter than 2 characters "
                "-- leading and trailing spaces don't count."
            )

        if len(stripped) > 48:
            raise ValueError("display name may NOT be longer than 48 characters.")

        return stripped

    @validator("passphrase")
    def validate_passphrase(cls, passphrase, values):  # noqa: E0213

        if len(passphrase) < 10:
            raise ValueError("passphrase may NOT be shorter than 10 characters.")

        if len(passphrase) > 64:
            raise ValueError("passphrase may NOT be longer than 64 characters.")

        user_inputs = []
        word_break_pattern = re.compile(r"\W+")
        interesting_keys = {"display_name", "email", "username"}
        interesting_values = (v for k, v in values.items() if k in interesting_keys)
        user_inputs = [
            part
            for value in interesting_values
            for part in re.split(word_break_pattern, value)
            if part
        ]

        temp = zxcvbn(passphrase, user_inputs=user_inputs)
        if temp["score"] < 3:
            error_messages = (
                line
                for line in [
                    temp["feedback"]["warning"],
                    *temp["feedback"]["suggestions"],
                ]
                if line
            )
            raise ValueError("\n".join(error_messages))

        return passphrase
