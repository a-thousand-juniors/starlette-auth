from starlette.routing import Route

from . import endpoints

routes = [
    Route(
        "/users", endpoint=endpoints.create_user, methods=["POST"], name="user_creation"
    ),
]
