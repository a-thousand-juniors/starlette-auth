from datetime import timezone

import pytest
from sqlalchemy import func, select
from sqlalchemy.sql.expression import desc

from starlauth import utils
from starlauth.app import app
from starlauth.resources import db
from starlauth.tables import users


@pytest.mark.asyncio
async def test_create_user(client, mocker, inception, passphrase_hash):
    # mocks
    utcnow_mock = mocker.patch(
        "starlauth.utils.get_utcnow", return_value=inception, autospec=True
    )
    passphrase_hash_mock = mocker.patch(
        "starlauth.utils.get_passphrase_hash",
        return_value=passphrase_hash,
        autospec=True,
    )
    # initial data
    payload = {
        "email": "hello@world.me",
        "username": "HelloWorld",
        "passphrase": "I said HELLO, world!",
        "display_name": "...and that's on Mary and her little lamb",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 201
    utcnow_mock.assert_called_once()
    passphrase_hash_mock.assert_called_once_with(payload["passphrase"])
    # user count
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count + 1
    # created user
    user_select_stmt = select([users]).order_by(desc(users.c.created_at))
    created_user = await db.fetch_one(user_select_stmt)
    assert created_user["username"] == payload["username"]
    assert created_user["casefolded_username"] == payload["username"].casefold()
    assert created_user["email"] == payload["email"]
    assert created_user["display_name"] == payload["display_name"]
    assert (
        created_user["created_at"].replace(tzinfo=timezone.utc)
        == utcnow_mock.return_value
    )
    assert created_user["passphrase"] == passphrase_hash
    # response data
    data = response.json()["data"]
    assert data["username"] == payload["username"]
    assert data["email"] == payload["email"]
    assert data["display_name"] == payload["display_name"]


@pytest.mark.asyncio
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_non_unique_email(client):
    email = "hello@world.me"
    user_insertion_stmt = users.insert().values(  # noqa: E1120
        email=email,
        username="firstUser",
        casefolded_username="firstuser",
        passphrase=utils.get_passphrase_hash("This is my 3rd passphrase"),
        display_name="Bink says boo!",
        created_at=utils.get_utcnow(),
    )
    await db.execute(user_insertion_stmt)
    payload = {
        "email": email,
        "username": "HelloWorld",
        "passphrase": "I said HELLO, world!",
        "display_name": "...and that's on Mary and her little lamb",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 409
    error = response.json()["error"]
    assert error["type"] == "uniqueness constraint violation"
    assert error["field"] == "email"
    assert error["message"] == "a user with this email address already exists"
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "username, duplicate_username",
    [
        ("HelloWorld", "HelloWorld"),  # exact duplicate
        ("HelloWorld", "helloworld"),  # lowercase variant
        ("HelloWorld", "hELloWoRld"),  # mixed-case variant
    ],
)
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_non_unique_username(
    client, username, duplicate_username
):
    user_insertion_stmt = users.insert().values(  # noqa: E1120
        email="hello@world.me",
        username=username,
        casefolded_username=username.casefold(),
        passphrase=utils.get_passphrase_hash("This is my 3rd passphrase"),
        display_name="Bink says boo!",
        created_at=utils.get_utcnow(),
    )
    await db.execute(user_insertion_stmt)
    payload = {
        "email": "goodbye@world.us",
        "username": duplicate_username,
        "display_name": "...and that's on Mary and her little lamb",
        "passphrase": "I said HELLO, world!",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 409
    error = response.json()["error"]
    assert error["type"] == "uniqueness constraint violation"
    assert error["field"] == "username"
    assert error["message"] == "a user with this username already exists"
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "invalid_email",
    [
        "invalidemail",
        "invalidemail.com",
        "invalid@email",
        "an@invalid@email.com",
        "in valid@email.com",
        " invalid@email.com",
        "invalid@email.com ",
        "invаlid@email.com",  # confusable local-part (the a)
        "invalid@gmаil.com",  # confusable domain (the a)
    ],
)
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_invalid_email(client, invalid_email):
    payload = {
        "email": invalid_email,
        "username": "HelloWorld",
        "passphrase": "I said HELLO, world!",
        "display_name": "...and that's on Mary and her little lamb",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 400
    error = response.json()["errors"][0]
    assert error["type"] == "value error"
    assert error["field"] == "email"
    assert error["message"] != ""
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "invalid_username",
    [
        "hello world",  # contains non-alphanumeric character
        "hellðworld",  # contains non-ASCII character (the ð)
        "hey",  # too short ( < 4 chars)
        "extraterrestrials",  # too long ( > 16 chars)
        "portfolio",  # reserved name
        "confusаble",  # confusable character (the a)
    ],
)
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_invalid_username(client, invalid_username):
    payload = {
        "email": "hello@world.me",
        "username": invalid_username,
        "passphrase": "I said HELLO, world!",
        "display_name": "...and that's on Mary and her little lamb",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 400
    error = response.json()["errors"][0]
    assert error["type"] == "value error"
    assert error["field"] == "username"
    assert error["message"] != ""
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "invalid_display_name",
    [
        "i",  # too short ( < 2 chars)
        " i\n\t",  # too short ( < 2 chars when stripped of padding)
        "Hello darkness, my old friend, I've come to ta...",  # too long ( > 48 chars)
    ],
)
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_invalid_display_name(client, invalid_display_name):
    payload = {
        "email": "hello@world.me",
        "username": "HelloWorld",
        "passphrase": "I said HELLO, world!",
        "display_name": invalid_display_name,
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 400
    error = response.json()["errors"][0]
    assert error["type"] == "value error"
    assert error["field"] == "display_name"
    assert error["message"] != ""
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "invalid_passphrase",
    [
        "helloworld",  # scores 0 on zxcvbn with related user inputs
        "qwerty12345",  # scores 1
        "Hello Mary",  # scores 2
        "&y short?",  # too short ( < 8 chars)
        "when i find myself in times of trouble "
        "mother mary calls to me...",  # too long ( > 64 chars)
    ],
)
@pytest.mark.usefixtures("mock_utcnow_and_passphrase_hasher")
async def test_failure_response_for_invalid_passphrase(client, invalid_passphrase):
    payload = {
        "email": "hello@world.me",
        "username": "HelloWorld",
        "passphrase": invalid_passphrase,
        "display_name": "...and that's on Mary and her little lamb",
    }
    path = app.url_path_for("user_creation")
    user_count_stmt = select([func.count()]).select_from(users)
    old_user_count = await db.execute(user_count_stmt)

    response = await client.post(path, json=payload)

    assert response.status_code == 400
    error = response.json()["errors"][0]
    assert error["type"] == "value error"
    assert error["field"] == "passphrase"
    assert error["message"] != ""
    new_user_count = await db.execute(user_count_stmt)
    assert new_user_count == old_user_count
